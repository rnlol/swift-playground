// Playground - noun: a place where people can play

import Cocoa

/*#######################################################################
                    CLASSES AND STRUCTURES
########################################################################*/

/*
Swift does not require you to create seperate interface and implementation files for classes and structs.

Instance of a class is called object.


*/

/************************************************************************
COMPARING CLASSES AND STRUCTURE
*************************************************************************/

/*
Common:
1. Define properties to store values
2. Define methods to provide functionality
3. Define subscripts to provide access to their values using subscript syntax
4. Define initializers to set up their initial state
5. Be extended to expand their functionality beyond default implementation.

Classes additional capabilities:
1. Inheritance
2. Type casting
3. Deinitializers
4. Reference counting

Structures are always copied when they passed around in your code
*/

/************************************************************************
DEFINITIION SYNTAX
*************************************************************************/

/*
Classes uses class keyword
Structures uses struct keyword

Use UpperCamelCase for class names //SomeClass, SomeStructure
Use lowerCamelCase for properties and methods //frameRate and IncrementCount
*/
//e.g.
class SomeClass {
    //class definition here
}
struct SomeStructure {
    //structure definition goes here
}

//e.g.
//This struct has 2 stored properties
//Stored properties are constants or variables that are bundled up and stored as part of the class or structure
struct Resolution {
    var width = 0
    var height = 0
}

//This class has 4 variable stored properties
class VideoMode {
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name: String?
}

/************************************************************************
                CLASSES AND STRUCTURE INSTANCES
*************************************************************************/

//Classes and structure instance
let someResolution = Resolution()
let someVideoMode = VideoMode()
//simplest way of initializer syntax type name of the class or struct followed by an empty parentheses ()


/************************************************************************
                    ACCESSING PROPERTIES
*************************************************************************/

//Using DOT SYNTAX to ACCESS the properties
println("The width of someResolution is \(someResolution.width)")
//prints "The width of someResolution is 0"
someResolution.width

//Accessing sub properties
println("The width of someVideoMode is \(someVideoMode.resolution.width))")
//prints "The width of someVideoMode is 0"
someVideoMode.resolution.width

//Using DOT SYNTAX to ASSIGN a new value to a variable property
someVideoMode.resolution.width = 1280
println("The width of someVideoMode is now \(someVideoMode.resolution.width)")
// prints "The width of someVideoMode is now 1280"
someVideoMode.resolution.width


/************************************************************************
                MEMBERWISE INITIALIZER FOR STRUCTURE TYPES
*************************************************************************/

//Initial  values for the properties of the new instance can be passed to the member wise initializer
let vga = Resolution(width: 640, height: 480) //memberwise initializer


/************************************************************************
                STRUCTURE AND ENUMERATION ARE VALUE TYPES
*************************************************************************/

/*
    A VALUE TYPE is a type whose value is COPIED when it is assigned to a variable or constant.

    Any structure and enumeration instances you create -- and any value types they have as properties -- are always copied when they are passed around in your code
 */

let hd = Resolution(width: 1920, height: 1080)
var cinema = hd //passed by value, new copy is assigned to cinema
cinema.width = 2048
cinema.width
hd.width
//cinema is updated with the new value and hd remains the same because this are two separate instances.

//The same with enumerations
enum CompassPoint {
    case North, South, East, West
}
var currentDirection = CompassPoint.West
let rememberedDirection = currentDirection
currentDirection = .East
if rememberedDirection == .West {
    println("The remembered direction is still .West")
}

/************************************************************************
                    CLASSES ARE REFERENCE TYPES
*************************************************************************/

/*
    Unlike value types, REFERENCE TYPES are NOT copied when they are assigned to a variable or constant, or when they are passed to function. Rather than a copy, a reference to the same existing instance is used.
 */

let tenEighty = VideoMode()
tenEighty.resolution = hd
tenEighty.interlaced = true
tenEighty.name = "1080i"
tenEighty.frameRate = 25.0

let alsoTenEighty = tenEighty
alsoTenEighty.frameRate = 30.0

tenEighty.frameRate //tenEighty is updated to 30.0

//Because classes are reference type tenEighty and alsoTenEighty refere to the same VideoMode instance.

/************************************************************************
                    IDENTITY OPERATORS
*************************************************************************/

/* 
    Identical to (===)
    Not identical to (!==)
 */

//Check if two constants or variable refer to exactly the same instance
if tenEighty === alsoTenEighty {
    println("tenEighty and alsoTenEighty refer to the same VideoMode instance")
}

/************************************************************************
                    CHOOSING BETWEEN CLASSES AND STRUCTURES
*************************************************************************/

/*
    //Structures are passed by VALUE - meaning when making a copy of the structure it creates a new separate instance

    //Classes are passed by REFERENCE - meaning when making a copy of class the copy is reference to the same instance

    GUIDELINES:
        1. The structure's primary purpose is to encapsulate a few relatively simple data values.
        2. It is reasonable to expect that the encapsulated values will be copied rather than referenced when you assign or pass around an instance of that structure.
        3. Any properties stored by the structures are themselves value types, which would also be expected to be copied rather than referenced.
        4. The structure does not need to inherit properties or behavior from another existing type.

    EXAMPLE OF GOOD CANDIDATES FOR STRUCTURE INCLUDE:
        1. The size of a geometric shape, perhaps encapsulating a WIDTH property and a HEIGHT property, both of type DOUBLE
        2. A way to refer ranges within a series, perhaps encapsulating a  START property and a LENGTH property.
        3. A point in a 3D coordinate system, perhaps encapsulating x, y, and z properties, each of type DOUBLE.
    
    In all other cases, define a class, and create instances of that class to be managed and passed by reference.
    In practice, this means that most custom data constructs should be classes, not structures

*/

/************************************************************************
    ASSIGNMENT AND COPY BEHAVIOR FOR STRINGS, ARRAYS, AND DICTIONARIES
*************************************************************************/

/*
    String, Array, and Dictionary are implemented as structures. This means that strings, arrays and dictioanries are copied when they are assigned to a new constant or variable.

    Objective C- NSString, NSArray, and NSDictionary in Foundation are classes. NSString, NSArray, and NSDictionary instances are always assigned and passed around as a reference to an existing instance, rather than a copy.
 */

/*#######################################################################
                        PROPERTIES
########################################################################*/
/*
    Properties associate values with a particular class, structure, or enumeration.

    TYPES OF PROPERTIES
    1. Stored
    2. Computed
 */

/************************************************************************
                        STORED PROPERTIES
*************************************************************************/

/*
    Stored properties is a constant or variable that is stored as part of an instance of a particular class or structure.

        Types of Stored Properties
    1. variabe stored properties - var keyword
    2. constant stored properties - constant keyword

 */

//e.g.
//Structure which describes a range of integers whose range length cannot be changed once it's created:
struct FixedLengthRange {
    var firstValue: Int //variable stored property
    let length: Int //constant stored property
}
var rangeOfThreeItems = FixedLengthRange(firstValue: 0, length: 3)
// the range represents integer values 0, 1, and 2
rangeOfThreeItems.firstValue = 6
// the range represents integer values 6, 7, 8


/************************************************************************
            STORED PROPERTIES OF CONSTANT STRUCTURE INSTANCE
*************************************************************************/

//e.g.
let rangeOfFourItems = FixedLengthRange(firstValue: 0, length: 4)
//this range represents integer values 0, 1, 2, and 3
//rangeOfFourItems.firstValue = 6 //this will return an error

//When an instance of a value type is marked as a constant, so are all of it's properties.

/************************************************************************
                        LAZY STORED PROPERTIES
*************************************************************************/

/*
    A lazy stored property is a property whose initial value is not calculated until the first time it is used.

    You indicate a lazy stored property by writing the @lazy modifier

    @lazy property must always be declared as a var keyword
 */

//e.g.
class DataImporter {
    /*
    DataImporter is a class to import data from an external file.
    The class is assumed to take non-trivial amount of time to initialize.
    */
    
    var fileName = "data.txt"
}

class DataManager {
    lazy var importer = DataImporter()
    var data = [String]()
    //The DataManager class would provide data management functionality here
}

let manager = DataManager()
manager.data += ["Some data"] // or manager.data.append("Some data")
manager.data += ["Some more data"]

// the DataImportedr instance for the importer property has not yet been created.

manager.importer.fileName
//the DataImporter instance for the importer property has now been created.
//only created when the importer property is first accessed.

/************************************************************************
                    COMPUTED PROPERTIES
*************************************************************************/

//Computed properties provide a getter and a setter to retrieve and set other properties and values indirectly.

//e.g.
struct Point {
    var x = 0.0, y = 0.0
}
struct Size {
    var width = 0.0, height = 0.0
}
struct Rect {
    var origin = Point()
    var size = Size()
    var center: Point {
    get {
        let centerX = origin.x + (size.width / 2)
        let centerY = origin.y + (size.height / 2)
        return Point(x: centerX, y: centerY)
    }
    set(newCenter) {
        origin.x = newCenter.x - (size.width / 2)
        origin.y = newCenter.y - (size.height / 2)
    }
    }
}

var square = Rect(origin: Point(x: 0.0, y: 0.0), size: Size(width: 10.0, height: 10.0))
let initialSquareCenter = square.center
square.center = Point(x: 15.0, y: 15.0)
println("the square.origin is now at (\(square.origin.x), \(square.origin.y))")


/*
        Example defines 3 structures
    1. Point encapsulates an (x, y) coordinate
    2. Size encapsulates a width and a height
    3. Rect defines a rectangle by an origin and a size
 */

/************************************************************************
                        SHORTHAND SETTER DECLARATION
*************************************************************************/

/*
    If a computer property's setter does not define a name for the new value to be set a default name newValue si used.
 */

struct AlternativeRect {
    var origin = Point()
    var size = Size()
    var center: Point {
    get {
        let centerX = origin.x + (size.width / 2)
        let centerY = origin.y + (size.height / 2)
        return Point(x: centerX, y: centerY)
    }
    set {
        origin.x = newValue.x - (size.width / 2) //newValue is the default name for the setter's parameter
        origin.y = newValue.y - (size.height / 2)
    }
    }
}

/************************************************************************
                    READ-ONLY COMPUTED PROPERTIES
*************************************************************************/

/*
    Read only can be declared by removing the get keyword and it's braces
 */
//e.g.
struct Cuboid {
    var width = 0.0, height = 0.0, depth = 0.0
    var volume: Double { //read-only property
    return width * height * depth
    }
}
let fourByFiveByTwo = Cuboid(width: 4.0, height: 5.0, depth: 2.0)
println("the volume of fourByFiveByTwo is \(fourByFiveByTwo.volume)")


/************************************************************************
                        PROPERTY OBSERVERS
*************************************************************************/

/*
    Propperty observers observe and respond to changes in a property's value.

    * willSet is called just before the value is stored.
    * didSet is called immediately after the new value is stored.

    * willSet and didSet parameters are constant
    * willSet's default parameter is newValue
    * didSet's default parameter is oldValue

    * If you assign a new value to a property within it's own didSet, the new value you assign will replace the one that was just set.

 */
//e.g.
class StepCounter {
    var totalSteps: Int = 0 {
    willSet(newTotalSteps) {
        println("About to set totalSteps to \(newTotalSteps)")
    }
    didSet {
        if totalSteps > oldValue {
            println("Added \(totalSteps - oldValue) steps")
        }
    }
    }
}

let stepCounter = StepCounter()
stepCounter.totalSteps = 200
// About to set totalSteps to 200
// Added 200 steps
stepCounter.totalSteps = 360
// Aout set totalSteps to 360
// Added 160 steps

/************************************************************************
                GLOBAL AND LOCAL VARIABLES
*************************************************************************/

/*
    Capabilities for computing and observing properties are also available to global variables and local variables.
 */


/************************************************************************
                        TYPE PROPERTIES
*************************************************************************/

/*
    - There can only be one copy of type properties no matter how many instances of that type is created.
    
    - useful for defining values that are universal to all instances of a particular type

 */

/************************************************************************
                        TYPE PROPERTY SYNTAX
*************************************************************************/

/*
    Define type for value type properties with 'static' keyword
    Define type for class type properties with 'class' keyword
 */

struct SomeTypeStructure {
    static var storedTypeProperty = "Some value."
    static var computedTypeProperty: Int {
    // return an Int value here
    return 1
    }
}

enum SomeEnumeration {
    static var storedTypeProperty = "Some value"
    static var computedTypeProperty: Int {
    return 1
    }
}

class SomeTypeClass {
    class var computedTypeProperty: Int {
    return 1
    }
}

//You can also define read-write computed type properties

/************************************************************************
                QUERYING AND SETTING THE TYPE PROPERTIES
*************************************************************************/

/* 
    Type properties are queried and set with dot syntax, just like instance properties.

    Type properties are queried and set on the type, not on an instance of that type
 */
//e.g.
println(SomeTypeClass.computedTypeProperty)
SomeTypeClass.computedTypeProperty
//instead of creating an instance and setting the propery you can directly set it to the type property.

println(SomeTypeStructure.storedTypeProperty)
SomeTypeStructure.storedTypeProperty
SomeTypeStructure.storedTypeProperty = "Another value"
println(SomeTypeStructure.storedTypeProperty)
SomeTypeStructure.storedTypeProperty
//Can only be accesed by using (Struct/Class/Enum).theProperty

//e.g. AUDIO CHANNEL
struct AudioChannel {
    static let thresholdLevel = 10
    static var maxInputLevelForAllChannels = 0
    var currentLevel: Int = 0 {
    didSet {
        if currentLevel > AudioChannel.thresholdLevel {
            //cap the new audio level to the threshold level
            currentLevel = AudioChannel.thresholdLevel
        }
        if currentLevel > AudioChannel.maxInputLevelForAllChannels {
            AudioChannel.maxInputLevelForAllChannels = currentLevel
        }
    }
    }
}

//Creating two new audio channels left channel and right channel
var leftChannel = AudioChannel()
var rightChannel = AudioChannel()

leftChannel.currentLevel = 7
println(leftChannel.currentLevel)
leftChannel.currentLevel
AudioChannel.maxInputLevelForAllChannels //there is only one copy of the type property no matter how many instance is created
rightChannel.currentLevel = 11
rightChannel.currentLevel
AudioChannel.maxInputLevelForAllChannels //Also is set to 10

/*#######################################################################
                        METHODS
########################################################################*/

/*
    Methods are associated with a particular type.
    Classes, structures and enums can have instance and type methods
 */

/************************************************************************
                    INSTANCE METHODS
*************************************************************************/

/*
    Instance methods are functions that belong to instances of a particular class, structure, or enumeration.

    Instance methods has implicit access to all other instance method and properties of that type.
 */
//e.g. COUNTER CLASS
//counter class defines three instance methods
class Counter {
    var count = 0 //stored properties
    func increment() { //1. increment the counter by 1
        count++
    }
    func incrementBy(amount: Int) {//2. increment the counter by an specfied integer
        count += amount
    }
    func reset() { // 3. resets the counter to zero
        count = 0
    }
}

let counter = Counter()
// the initial counter value is 0
counter.increment()
// the counter value is now 1
counter.incrementBy(5)
// the counter values is now 5
counter.reset()
// the counter value is now 0

/************************************************************************
                    LOCAL AND EXTERNAL PARAMETER NAMES for METHODS
*************************************************************************/

/*
    Methods are just functions associated with a type

    Swift gives the first parameter a local parameter name by default and gives the second and subsequent parameter names bot local and external parameter names
 */

class OtherCounter {
    var count: Int = 0
    func incrementBy(amount: Int, numberOfTimes: Int) {
        count += amount * numberOfTimes
    }
}

let otherCounter = OtherCounter()
otherCounter.incrementBy(5, numberOfTimes: 3)
// counter value is now 15

/*
    This default behavior effectively treats the method as if you had written a hash symbol #before the numberOfTimes parameter

    func incrementBy(amount: Int, #numberOfTimes: Int) {
        count += amount * numberOfTimes
    }
 */

/************************************************************************
        MODIFYING EXTERNAL PARAMETER NAMES for METHODS
*************************************************************************/

/*
    Sometime's it's useful to provide an external parameter name for a method's first parameter even though this is not the default behavior.

    You can do this by adding # before the parameter name
 */

/************************************************************************
            THE SELF PROPERTY
*************************************************************************/

//Every instance of the type has a property called 'self', which is equivalent to the instance itself
//Self is more qualified to use inside instance methods with the same parameter names
//e.g.
struct SelfPoint {
    var x = 0.0, y = 0.0
    func isToTheRightOfX(x: Double) -> Bool {
        return self.x > x
    }
}

let somePoint = SelfPoint(x: 4.0, y: 5.0)
if somePoint.isToTheRightOfX(1.0) {
    println("This point is to the right of the line where x == 1.0")
}

/************************************************************************
            MODIFYING VALUE TYPES from WITHIN INSTANCE METHODS
*************************************************************************/

/*
    Structure and enumeration are value types. By default, the properties of a value type cannot be modified from within its instance methods.

    
    If you want to modify the properties of you structure or enumeration within a particular method, you can opt in to use 'mutating' behavior for that method.
 */

/*
//This will return a compile-time error

struct sample{
    var x = 0.0, y = 0.0
    func updateX(x: Double) {
        self.x = x
    }
}
*/

//e.g.
struct MutatingPoint {
    var x = 0.0, y = 0.0
    mutating func moveByX(deltaX: Double, y deltaY: Double) {
        x += deltaX
        y += deltaY
    }
}
var someMutatingPoint = MutatingPoint(x: 1.0, y: 1.0)
someMutatingPoint.moveByX(2.0, y: 3.0)
println("The point is now at (\(someMutatingPoint.x), \(somePoint.y))")
// prints "The point is now at (3.0, 4.0)"
var anotherMutatingPoint = someMutatingPoint
anotherMutatingPoint.moveByX(5.0, y: 9.0)
anotherMutatingPoint.x
anotherMutatingPoint.y

/************************************************************************
           ASSIGNING TO SELF WITHIN A MUTATING METHOD
*************************************************************************/

/*
    Mutating methods can assign an entirely new instance to the implicit 'self' property
 */
//e.g
struct AssignMutatingPoint {
    var x = 0.0, y = 0.0
    mutating func moveByX(deltaX: Double, y deltaY: Double) {
        self = AssignMutatingPoint(x: x + deltaX, y: y + deltaY) //a new instance of mutatingPoint is assing to self
    }
}

//ENUM EXAMPLE
enum TriStateSwitch {
    case Off, Low, High
    mutating func next() {
        switch self {
        case Off:
            self = Low
        case Low:
            self = High
        case High:
            self = Off
        }
    }
}
var ovenLight = TriStateSwitch.Low
ovenLight.next()
// ovenLight is now equal to .High
ovenLight.next()
// ovenLight is now equal to .Off


/************************************************************************
                        TYPE METHODs
*************************************************************************/

/*
    Instance methods are methods called on an instance of a particular type.
    Type methods are methods called on the type itself.
 */

//e.g. Example format
class SomeTypeMethodClass {
    class func someTypeMethod() {
        // type method implementation here
    }
}
SomeTypeMethodClass.someTypeMethod()


//e.g. LEVEL TRACKER -tracks a player's progress through the different levels or stages of a game.
struct LevelTracker {
    static var highestUnlockedLevel = 1
    static func unlockedLevel(level: Int) {
        if level > highestUnlockedLevel { highestUnlockedLevel = level } // can access the static property by using the static property's name without a type name prefix.
    }
    static func levelIsUnlocked(level: Int) -> Bool {
        return level <= highestUnlockedLevel //access highestUnlockedLevel because it is a type method
    }
    var currentLevel = 1
    mutating func advanceToLevel(level: Int) -> Bool {
        if LevelTracker.levelIsUnlocked(level) {
            currentLevel = level //update current level because it is a mutating method
            return true
        } else {
            return false
        }
    }
}

//e.g. PLAYER CLASS - track and update the progress of an individual player
class Player {
    var tracker = LevelTracker()
    let playerName: String
    func completedLevel(level: Int) {
        LevelTracker.unlockedLevel(level + 1)
        tracker.advanceToLevel(level + 1)
        
    }
    init(name: String) { //if initializer is not inserted, you will encoutner a compile time error unless you put an inital value to 'playerName' upon declaration
        playerName = name
    }
}

var player = Player(name: "Argyrios")
player.completedLevel(1)
//Highest unlocked level is now
LevelTracker.highestUnlockedLevel

player = Player(name: "Beto")
if player.tracker.advanceToLevel(6) {
    println("player is now on level 6")
} else {
    println("level 6 has not yet been unlocked")
}
// prints level 6 has not yet been unlocked

/*#######################################################################
                                SUBSCRIPTS
########################################################################*/

/*
    Subscripts are shortcuts for accessing the member elements of a collection, list, or sequence.

    You use subscripts to set and retrieve values by index witout needing seperate methods for setting and retireval.

    e.g.
        Array - someArray[index]
        Dictionary - someDictionary[key]

 */

/************************************************************************
                                SUBSCRIPT SYNTAX
*************************************************************************/

/*
    //Read Write Syntax
    subscript(index: Int) -> Int {
        get {
            //return appropriate subscript value here
        }
        set(newValue) {
            // perform a suitable setting action here
        }
    }

    //Read-only
    subscript(index: Int) -> Int {
    
    }
 */

//e.g TIMES TABLE
struct TimesTable {
    let multiplier: Int
    subscript(index: Int) -> Int {
        return multiplier * index
    }
}
let threeTimesTable = TimesTable(multiplier: 3)
threeTimesTable[6]

/************************************************************************
                            SUBSCRIPT USAGE
*************************************************************************/

/*
    Subscripts are typically used as a shortcut for accessing the member elements in a collection, list, or sequence.
 */


/************************************************************************
                              SUBSCRIPT OPTIONS
*************************************************************************/

/*
    Subscript overloading - having multiple subscripts
 */

//e.g. Matrix  - 2 Integer Parameters
struct Matrix {
    let rows: Int, columns: Int
    var grid: [Double]
    init(rows: Int, columns: Int) {
        self.rows = rows
        self.columns = columns
        grid = Array(count: rows * columns, repeatedValue: 0.0)
    }
    func indexIsValidForRow(row: Int, column: Int) -> Bool {
        return row >= 0 && row < rows && column >= 0 && column < columns
    }
    subscript(row: Int, column: Int) -> Double {
        get {
            assert(indexIsValidForRow(row, column: column), "Index out of range 123")
            return grid[(row * columns) + columns]
        }
        set {
            assert(indexIsValidForRow(row, column: column), "Index out of range")
            grid[(row * columns) + column] = newValue//e.g. 0 * 3 = 0 + 1
        }
    }
}


var matrix = Matrix(rows: 2, columns: 2)
matrix[0, 1] = 1.5
matrix[1, 0] = 3.2
matrix

//let someValue = matrix[2, 2]
// this trigger an assert because [2, 2] is out fo the matrix bounds


/*#######################################################################
                                INHERITANCE
########################################################################*/

/*
    a CLASS inherits methods, properties, and other characteristics from another class.

    SUBCLASS - one who inherits from another class
    SUPERCLASS - the class the subclass inherits to

    Clasess in swift can call and access methods, properties, and subscripts belonging to their SUPERCLASS and can provide their own overriding versions of those methods.
    
    Classes can also add property observers to inherited properties.

 */


/************************************************************************
                            DEFINING a BASE CLASS
*************************************************************************/

/*
    BASE CLASS - any class that does not inherit from another class
 */
//e.g. Vehicle
class Vehicle {
    var numberOfWheels: Int
    var maxPassengers: Int
    func description() -> String {
        return "\(numberOfWheels) wheels; up to \(maxPassengers) passengers"
    }
    //Can also be done like this
    var descriptionVar: String {
        return "\(numberOfWheels) wheels; up to \(maxPassengers) passengers"
    }
    init() {
        numberOfWheels = 0
        maxPassengers = 1
    }
    
}

//Simplest form of initializer
/* 

init() {
    
}

The same as let someClass = SomeClass()
*/

/************************************************************************
                            SUBCLASSING
*************************************************************************/

/*
    Subclassing is the act of basing a new class on an existing class. It inherits characteristics from the existing class.

    class SomeClass: SomeSuperClass {
        //class definition goes here
    }
 */

//e.g. BICYLE class
class Bicycle: Vehicle {
    override init() { //All subclass when adding an init must add override init.
        super.init() //super.init is called to ensure all properties are inialized before Bicycle modifies them
        numberOfWheels = 2
        
    }
}

let bicycle = Bicycle()
println("Bicycle: \(bicycle.description())")
// Bicycle: 2 wheels; up to 1
bicycle.description()
bicycle.descriptionVar


//Subclasses can themselves be subclassed
//e.g. TANDEM: Tandem has the same number of wheels but is a two seater.
class Tandem: Bicycle {
    override init() {
        super.init()
        maxPassengers = 2
    }
}

//creating an instance
let tandem = Tandem()
tandem.description()
tandem.descriptionVar


/************************************************************************
                                OVERRIDING
*************************************************************************/

/*
    Subclass can provide it's own custom implementation of an instance method, class method, instance property, or subscript that would otherwise inherit from a superclass. This is known as OVERRIDING.

    Use the 'override' method to override characteristics.
 */

/************************************************************************
        ACCESSING SUPERCLASS METHODS, PROPERTIES, AND SUBSCRIPTS
*************************************************************************/

/*
    Method - super.someMethod()
    Property - super.someProperty
    Subscript - super[someIndex]
*/

/************************************************************************
                        OVERRIDING METHODS
*************************************************************************/

class Car: Vehicle {
    var speed: Double = 0.0
    override init() { //must add override init for subclass whose superclass has init
        super.init()
        maxPassengers = 5
        numberOfWheels = 4
    }
    override func description() -> String { //override description method from super class
        return super.description() + "; " + "travelling at \(speed) mph"
    }
}

let car = Car()
println("Car: \(car.description())")
car.description()

/************************************************************************
                OVERRIDING PROPERTY GETTERS AND SETTERS
*************************************************************************/

/*
    You can present an inherited read-only property as read-write property by providing both getter and setter in your SUBCLASS property OVERRIDe.

    You cannot present an inherited read-write property as a ready-only property.

 */

//e.g.
class SpeedLimitedCar: Car {
    override var speed: Double {
    get {
        return super.speed
    }
    set {
        super.speed = min(newValue, 40.0)
    }
    }
}

let limitedCar = SpeedLimitedCar()
limitedCar.speed = 60.0
println("SpeedLimitedCar: \(limitedCar.description())")
// SpeedLimitedCar: 4 wheels; up to 5 passengers; travelling at 40.0 mph
limitedCar.description()
limitedCar.speed

/************************************************************************
                    OVERRIDING PROPERTY OBSERVERS
*************************************************************************/

/*
    You cannot add property observers to inherited constant stored properties or inherited read-only computed properties
 */

class AutomaticCar: Car {
    var gear = 1
    override var speed: Double { //add a property observer to speed to automatically sets the gear property
    didSet {
        gear = Int(speed / 10.0) + 1
    }
    }
    override func description() -> String {
        return super.description() + " in gear \(gear)"
    }
}

let automatic = AutomaticCar()
automatic.speed = 35.0
println("AutomaticCar: \(automatic.description())")
// Automatic: 4 wheels; up to 5 passengers; traveling at 35.0 mph in gear 4
automatic.description()

/************************************************************************
                    PREVENTING OVERRIDES
*************************************************************************/

/*
    You can prevent a method, property, or subscript from being overridden by marking it as 'final'

    //e.g
    final var, final func, final class func, final subscript
 */


/*#######################################################################
                            INITIALIZATION
########################################################################*/

/*
    Initialization is the process of preparing an instance of a class, structure, or enumeration fo use.

 */

/************************************************************************
                     SETTING INITIAL VALUES FOR STORED PROPERTIES
*************************************************************************/

//Classes and structures must set all of their stored properties to an appropriate initial value by the time an instance of the class or structure is created.


/************************************************************************
                            INITIALIZERS
*************************************************************************/

//Initializers are called to create a new instance of a particular type.

//written using the 'init' keyword

//e.g. FAHRENHEIT
struct Fahrenheit {
    var temperature: Double
    init() {
        temperature = 32.0 //set initial value of temperature here
    }
}

var f = Fahrenheit()
println("The default temperature is \(f.temperature) Fahrenheit")
f.temperature


/************************************************************************
                          DEFAULT PROPERTY VALUES
*************************************************************************/

//If the property always take the same initial value, provide a default value rather setting a value within an initializer.

struct DefaultFahrenheit {
    var temperature = 32.0 // this is the default property value
}

let defaultFahrenheit = DefaultFahrenheit()
defaultFahrenheit.temperature


/************************************************************************
                         CUSTOMIZING INITIALIZATION
*************************************************************************/


/************************************************************************
                        INITIALIZATION PARAMETERS
*************************************************************************/

//e.g. Two custom initializers and initialization parameter
struct Celsius {
    var temperatureInCelsius: Double
    init(fromFahrenheit fahrenheit: Double) { //initialization parameter named 'fahrenheit' with external name 'fromFahrenheit'
        temperatureInCelsius = (fahrenheit - 32.0) / 1.8
    }
    init(fromKelvin kelvin: Double) { //initialization parameter named 'kelvin' with external name 'fromKelvin'
        temperatureInCelsius = kelvin - 273.15
    }
}
let boilingPointOfWater = Celsius(fromFahrenheit: 212.0)
let freezingPointOfWater = Celsius(fromKelvin: 273.15)


/************************************************************************
                    LOCAL AND EXTERNAL PARAMETER NAMES
*************************************************************************/

/*
    Swift provide an automatic external name for every parameter in an initializer if no external name is provided.
 */

//e.g. COLOR
struct Color {
    let red, green, blue: Double
    init(red: Double, green: Double, blue: Double) {
        self.red = red
        self.green = green
        self.blue = blue
    }
    init(white: Double) {
        red = white
        green = white
        blue = white
    }
}

//Creating a new color instance
let magenta = Color(red: 1.0, green: 0.0, blue: 1.0) //the external parameter name is the same as the local parameter name
let halfgray = Color(white: 0.5)

/************************************************************************
                INITIALIZER PARAMETERS WITHOUT EXTERNAL NAMES
*************************************************************************/

/*
    use underscore(_) if you don't want to use an external name
 */

struct Celsius2 {
    var temperatureInCelsius: Double
    init(fromFahrenheit fahrenheit: Double) { //initialization parameter named 'fahrenheit' with external name 'fromFahrenheit'
        temperatureInCelsius = (fahrenheit - 32.0) / 1.8
    }
    init(fromKelvin kelvin: Double) { //initialization parameter named 'kelvin' with external name 'fromKelvin'
        temperatureInCelsius = kelvin - 273.15
    }
    init(_ celsius: Double) { // uses _
        temperatureInCelsius = celsius
    }
}
let bodyTemperature = Celsius2(37.0)

/************************************************************************
                    OPTIONAL PROPERTY TYPES
*************************************************************************/

/*
    You can use optional type for your property if the stored property is allowed to have no value or no value at all during the initialization.
 */

//e.g. SURVEY QUESTION
class SurveyQuestion {
    var text: String
    var response: String? //optional value String? automatically assign a default value of nil
    init(text: String) {
        self.text = text
    }
    func ask() {
        println(text)
    }
}
let cheeseQuestion = SurveyQuestion(text: "Do you like cheese?")
cheeseQuestion.ask()
// prints "Do you like cheese?"
cheeseQuestion.response = "Yes, I do like cheese."

/************************************************************************
        MODIFYING CONSTANT PROPERTIES DURING INITIALIZATION
*************************************************************************/

/*
    You can modify the value of a constant property at any point during initialization

    For class instances, a constant property can only be modified during initialization by the class that introduces it. It cannot be modified by the subclass
 */

class SurveyQuestion2 {
    let text: String //constant with no value
    var response: String? //optional string
    init(text: String) {
        self.text = text //initialize a value to the constant
    }
    func ask() {
        println(text)
    }
}
let beetsQuestion = SurveyQuestion2(text: "How about beets?")
beetsQuestion.ask()
// prints "How about beets"
//beetsQuestion.text = "sample question" //compile time error 'text' has already been assigned.
beetsQuestion.response = "I also like beets. (But not with cheese.)"

/************************************************************************
                        DEFAULT INITIALIZERS
*************************************************************************/


//e.g. SHOPPING LIST ITEM
class ShoppingListItem {
    var name: String?
    var quantity = 1
    var purchased = false
}

//Because all of the properties have default values and it is a base class this example uses deafult initializers
var item = ShoppingListItem()

/************************************************************************
                MEMBERWISE INITIALIZERS for STRUCTURE TYPES
*************************************************************************/

//Structure types automatically receive a memberwise initializer unlike class that they still need an initializer

//e.g.
struct MemberSize {
    var width = 0.0, height = 0.0 //inferred of type Double
}
let twoByTwo = Size(width: 2.0, height: 2.0) //creates an initializer init(width:height)


/************************************************************************
                INITIALIZER DELEGATION FOR VALUE TYPES
*************************************************************************/

//Initializers can call other initializers to perform part of an instance initialization to avoid duplicating codes across multiple initializer.

//e.g.
struct Rect2 {
    var origin = Point()
    var size = Size()
    init() {}
    init(origin: Point, size: Size) {
        self.origin = origin
        self.size = size
    }
    init(center: Point, size: Size) {
        let originX = center.x - (size.width / 2)
        let originY = center.y - (size.height / 2)
        self.init(origin: Point(x: originX, y: originY), size: size) //Called other initializer to create the rect
    }
}
let basic = Rect2()
// basicRect's origin is (0.0, 0.0) and its size is (0.0, 0.0)
let originRect = Rect2(origin: Point(x: 2.0, y: 2.0), size: Size(width: 5.0, height: 5.0))
// originRect's origin is (2.0, 2.0) and its size is (5.0, 5.0)
let centerRect = Rect2(center: Point(x: 4.0, y: 4.0), size: Size(width: 3.0, height: 3.0))
// centerRect's origin is (2.5, 2.5) and it's size is (3.0, 3.0)


/************************************************************************
                CLASS INHERITANCE AND INITIALIZATIOn
*************************************************************************/

/*
    All of a class's stored properties -- including any properties the class inherits from it's superclass -- must be assigned an initial value during INITIALIZATION.
 */

/************************************************************************
            DESIGNATED INITIALIZERS and CONVENIENCE INITIALIZERS
*************************************************************************/

/*
    Designated initializers - are the primary initializer for a class. Fully initiatizes all properties introduced by that class.

    Every class must have at least one designated initializers.

    Convenience Initializers - are secondary, supporting initializer for a class. 
                            -You can define a convenience initializer to call a designated initializer from the same class
 */

/************************************************************************
                        INITIALIZER CHAINING
*************************************************************************/

//RULE 1
//A designated initializer must call a designated initializer from their immediate superclass

//RULE 2
//Convenience initializer must call another initializer available in the same class

//RULE 3
//convenience initializer must ultimately end up calling a designated initializer

// A simple way to remember this is:
// 1. Designated initializers must always delegate up
// 2. Convenience initializer must always delegate across.

/************************************************************************
                        TWO PHASE INITIALIZATION
*************************************************************************/

/*
        Class initialization in swift has 2 process:
    1. First Phase, each stored property is assigned an initial value by the class that introduced it.
    2. Once the initial state for every stored property has been determined, the second phase begins. Each class is given the opportunity to customized its stored property further before the new instance is considered ready for use.

 */

/*
                        SAFETY CHECKS
    1. A designated initializer must ensure that all properties introduced by its class are initialized before it delegates up to a super class.

    2. A designated initializer must delegate up to a superclass initializer before assigning a value to an inherited property.

    3. A convenience initializer must delegate to another initializer beforea assigning a value to any property.

    4. An initializer cannot call any instance methods,read the values of any instance properties, or refer to self as a value until after the first phase of initialization is complete.
 */

/************************************************************************
                    INITIALIZER INHERITCANCE AND OVERRIDING
*************************************************************************/

/*
    If the initializer you are overriding is a DESIGNATED INITIALIZER, you can override its implementation in your subclass and call the superclass version of the initializer from within your overriding version.

    If the initializer you are overriding is a CONVENIENCE INITIALIZER, your override must call another designated initializer from its own subclass
 */

/************************************************************************
                    AUTOMATIC INITIALIZERS
*************************************************************************/

/*
    RULE 1: If your subclass doesn't define any designated Initializers, it automatically inherits all of it's superclass designated initializers.

    RULE 2: If your subclass provides an implementation of all its superclass designated initializers --  either by inheriting them as per rule 1, or by providing a custom implementation as part of it's definition -- then it automatically inherits all of the superclass convenience initializers.
 */

/************************************************************************
            DESIGNATED AND CONVENIENCE INITIALIZERS in ACTION
*************************************************************************/

//e.g.
class Food {
    var name: String
    init(name: String) { //designated initializer
        self.name = name
        println("Food - init")
    }
    convenience init() { //convenience initializers must always end up calling a designated initializer
        println("Food - convenience init?")
        self.init(name: "[Unnamed]")
    }
}

//let namedMeat = Food(name: "Bacon")

//let mysteryMeat = Food()

class RecipeIngredient: Food {
    var quantity: Int
    init(name: String, quantity: Int) { //initializes the super class
        println("RecipeIngredient init")
        self.quantity = quantity //safety check 1
        super.init(name: name)
    }
    convenience override init(name: String) { //init is inherited from the super class. must add override if super class convenience init exists
        println("RecipeIngredient - convenience init: \(name)")
        self.init(name: name, quantity: 1)
    }
    // the definition of this convenience initializer makes RecipeIngredient instances quicker and more convenient to create, and avoids code duplication when creating several single-quantity RecipeIngredient instances.
    
    //the subclass inherits automatically inherits all of it's superclass 's convenience initializers by providing an implementation of all it's superclass designated initializers
}

//all three of these initializers can be used to create new RecipeIngredient

//inherits the init() initialzer from food and run the convenience init() in Recipe Ingredient
//Please think that the init() is also in the RecipeIngredient class
let oneMysteryItem = RecipeIngredient()
let oneBacon = RecipeIngredient(name: "Bacon")
let sixEggs = RecipeIngredient(name: "Eggs", quantity: 6)


//e.g. 
class ShoppingListItem2: RecipeIngredient {
    var purchased = false
    var description: String { //computed read-only property
    var output = "\(quantity) x \(name)"
        output += purchased ? " ✔" : " ✘"
        return output;
    }
}
//since initializers inherits other identifier
let shopItem = ShoppingListItem2() //init() -> init(name) -> init(name, quantity)

//three shoppingListItem instance
var breakfastList = [ShoppingListItem2(), ShoppingListItem2(name: "bacon"), ShoppingListItem2(name: "Eggs", quantity: 6)]
breakfastList[0].name = "Orange juice"
breakfastList[0].purchased = true
for item in breakfastList {
    println(item.description)
    item.description
}
//Because it provides a default value for all the properties and does not define any initializers itself, ShoppingListItem inherits all of the designated and convenience initializer from it's superclass.


/************************************************************************
                            REQUIRED INITIALIZERS
*************************************************************************/

//Write the 'required' modifier before the definition of a designated or convenience initializer on a class to indicate that every subclass of that class must implement the required initializer

//RULE 1
//If your superclass has a required designated initializer, you must provide an implementation of that initializer. The requirement cannot be satisfied by an inherited initializer.
//RULE 2
//If your superclass has a required convenience initializer, you can satisfy the requirement with an inherited initializer, even if the requirement started life as a designated initializer higher up the chain.

/************************************************************************
        SETTING A DEFAULT PROPERTY VALUE WITH A CLOSURE or FUNCTION
*************************************************************************/

/*
    class SomeClass {
        let someProperty: SomeType = {
        // create a default value for someProperty inside this closure
        // someValue must be of the same type as SomeType
        return someValue
        }()
    }
 */

//e.g. CHECKER BOARD - board colors with array of 100 bool true - black square, false - white square
struct Checkerboard {
    let boardColors: [Bool] = {
        var temporaryBoard = [Bool]()
        var isBlack = false
        for i in 1...10 {
            for j in 1...10 {
                temporaryBoard.append(isBlack)
                isBlack = !isBlack
            }
            isBlack = !isBlack
        }
        return temporaryBoard
    }()
    func squareisBlackAtRow(row: Int, column: Int) -> Bool {
        return boardColors[(row * 10) + column]
    }
}

let board = Checkerboard()
println(board.squareisBlackAtRow(0, column: 1))
// prints "true"
println(board.squareisBlackAtRow(9, column: 9))
// prints "false'

/*#######################################################################
                            DEINITIALIZATION
########################################################################*/

/*
    Deinitiliazer is called immediately before a class instance is deallocated.

    uses deinit keyword
 */

/************************************************************************
                        How Deinitialization Works
*************************************************************************/

/*
    Deinitializers are called automatically, just before instance deallocation takes place.
 */

/************************************************************************
                        Deinitializers in Action
*************************************************************************/

//e.g. BANK AND PLAYER

struct Bank {
    static var coinsInBank = 10_000 //type properties
    static func vendCoins(var numberOfCoinsToVend: Int) -> Int {
        numberOfCoinsToVend = min(numberOfCoinsToVend, coinsInBank)
        coinsInBank -= numberOfCoinsToVend
        return numberOfCoinsToVend
    }
    static func receiveCoins(coins: Int) {
        coinsInBank += coins
    }
}

class Player2 {
    var coinsInPurse: Int
    init(coins: Int) {
        coinsInPurse = Bank.vendCoins(coins)
    }
    func windCoins(coins: Int) {
        coinsInPurse += Bank.vendCoins(coins)
    }
    deinit {
        println("YEEEHA")
        Bank.receiveCoins(coinsInPurse)
    }
}

var playerOne: Player2? = Player2(coins: 100)
println("A new player has joined the game with \(playerOne!.coinsInPurse)") //optional unwrapping
playerOne!.coinsInPurse
println("There are now \(Bank.coinsInBank) coins left in the bank")
// prints "There are now 9900 coins left in the bank"

playerOne!.windCoins(2_000)
println("PlayerOne won 2000 coins & now has \(playerOne!.coinsInPurse) coins")
// prints "PlayerOne won 2000 coins & now has 2100 coins"
println("The bank now only has \(Bank.coinsInBank) coins left")
// prints "The bank now only has 7900 coins left"

playerOne = nil
println("Player has left the game")
// prints "PlayerOne has left the game"
println("The bank now has \(Bank.coinsInBank) coins")

//deinit does not work in playground

/*#######################################################################
                        AUTOMATIC REFERENCE COUNTING
########################################################################*/


/************************************************************************
                         ARC in Action
*************************************************************************/


//e.g.
class Person {
    let name: String
    init(name: String) {
        self.name = name
        println("\(name) is being initialized")
    }
    deinit {
        println("\(name) is being deinitialized")
    }
}

var reference1: Person?
var reference2: Person?
var reference3: Person?

reference1 = Person(name: "John Applesed")
reference2 = reference1
reference3 = reference1
//There are now 3 reference to this single person instance

reference1 = nil
reference2 = nil
// 2 strong reference are break

reference3 = nil
//prints "John Applesed is being deinitialized"

/************************************************************************
            Strong Reference Cycles Between Class Instances
*************************************************************************/

/*
    Strong reference cycle -  if two class instances hold a strong reference to each other, such that each instance keeps the other alive.

 */

//e.g. EXAMPLE OF STRONG REFERENCE CYCLE
class StrongPerson {
    let name: String
    init(name: String) {
        self.name = name
    }
    var apartment: Apartment?
    deinit {
        println("\(name) is being deinitialized")
    }
}

class Apartment {
    let number: Int
    init(number: Int) {
        self.number = number
    }
    var tenant: StrongPerson?
    deinit {
        println("Apartment #\(number) is being initialized")
    }
}

var john: StrongPerson?
var number73: Apartment?

john = StrongPerson(name: "John Appleseed") //john has now strong reference to StrongPerson instance
number73 = Apartment(number: 73) //number73 has now strong reference to number73 instance

john!.apartment = number73 //person has now a strong reference to apartment
number73!.tenant = john //apartment has now a strong reference to person

john = nil
number73 = nil
//When breaking these instances, reference do not drop to zero and instances are not deallocated by ARC because they have string references to each other
//Neither of the deinitializer are called when these two are set to nil, causing a memory leak in your app


/************************************************************************
        Resolving Strong Reference Cycles Between Class Instances
*************************************************************************/


/*
    Swift provides two ways to resolve strong reference cycles when you work with properties of class type: weak references and unowned references.

 */


/************************************************************************
                        Weak References
*************************************************************************/

/*
    A weak reference is a reference that does not keep a strong hold on the instance it refers to, and so does not stop ARC from disposing of the referenced instance.

    Use a weak reference to avoid reference cycles whenever it is possible for that reference to have “no value” at some point in its life.

    Weak references must be declared as variables, to indicate that their value can change at runtime. A weak reference cannot be declared as a constant.
 */

//e.g.
class Person2 {
    let name: String
    init(name: String) {
        self.name = name
    }
    var apartment: Apartment2?
    deinit {
        println("\(name) is being initialized")
    }
}

class Apartment2 {
    let number: Int
    init(number: Int) {
        self.number = number
    }
    weak var tenant: Person2?
    deinit {
        println("Apartment #\(number) is being deinitialized")
    }
}

var john2: Person2?
var number732: Apartment2?

john2 = Person2(name: "John Appleseed") // john2 has strong reference to Person2
number732 = Apartment2(number: 732) //number732 has strong reference to Apartment2

john2!.apartment = number732 //person has strong reference to apartment
number732!.tenant = john2 //apartment has weak reference to person

john2 = nil
//because there no more strong references to the Person instance, it is deallocated
// prints John Appleseed is being deinitialized

//Because there are no more strong references to the Apartment instance, it too is deallocated
number73 = nil
// prints "Apartment #73 is being deinitialized"


/************************************************************************
                        Unowned References
*************************************************************************/

/*
    Unowned reference does not keep a strong hold on the instance it refers to.

    Unowned reference is assumed to always have a value

    Use unowned references only when you are sure that the reference will always refer to an instance.
 */

//e.g Customer and CreditCard
class Customer {
    let name: String
    var card: CreditCard?
    init(name: String) {
        self.name = name
    }
    deinit {
        println("\(name) is being initialized")
    }
}

class CreditCard {
    let number: UInt64
    unowned let customer: Customer
    init(number: UInt64, customer: Customer) {
        self.number = number
        self.customer = customer
    }
    deinit {
        println("Card #\(number) is being deinitialized")
    }
}

var mark: Customer?
mark = Customer(name: "Mark Appleseed")
mark!.card = CreditCard(number: 1234_5678_9012, customer: mark!)
//Customer has a strong reference to Credit Card
//CreditCard has a unowned reference to Customer

mark = nil
// prints "Mark Appleseed is being deinitialized"
// prints "Card  #123456789012 is being deinitialized

//Because there are no more strong references to the Customer instance it deallocated. CreditCard Instance is also deallocated.


/************************************************************************
    Unowned References and Implicitly Unwrapped Optional Properties
*************************************************************************/

/*
    1. If two properties are allowed to be nil you can use weak references
    2. If one property is allowed to be nil and the other is not you ca use unowned references
 */

//3. When both properties should always have a value and neither property should always be nil once initialization is complete. In this scenario it's useful to combine an unowned property on one class and implicitly unwrapped optional property on the other class.

//e.g.
class Country {
    let name: String
    let capitalCity: City! //City! has default value of nil
    init(name: String, capitalName: String) {
        self.name = name
        self.capitalCity = City(name: capitalName, country: self)
    }
}
class City {
    let name: String
    unowned let country: Country
    init(name: String, country: Country) {
        self.name = name
        self.country = country
    }
}

//The initializer for City is called from within the initializer for Country. However, the initializer for Country cannot pass self to the City initializer until a new Country instance is fully initialized, as described in Two-Phase Initialization.

//Because capital city has default nil, a new country instance is considered fully initialize as soon as the country instance sets its name property within it's initializer.  Country inializer can now start to reference to it's implicit self.

var country = Country(name: "Canada", capitalName: "Ottawa")
println("\(country.name)'s capital city is called \(country.capitalCity.name)")


/************************************************************************
                Strong Reference Cycles for Closures
*************************************************************************/

/*
    Strong reference cycle can occurs if you assign a closure to a property of a class instance, and the body of that closure captures the instance.
 */

//e.g. An example that will show a closure having strong reference cycle
class HTMLElement {
    let name: String
    let text: String?
    
    lazy var asHTML: () ->String = {
        if let text = self.text { //self.text captures the instance of HTML Element at the same time the closure is assingn as a property of the class
            return "<\(self.name)>\(text)</\(self.name)>"
        } else {
            return "<\(self.name) />"
        }
    }
    
    init(name: String, text: String? = nil) {
        self.name = name
        self.text = text
    }
    
    deinit {
        println("\(name) is being deinitialized")
    }
}

var paragraph: HTMLElement? = HTMLElement(name: "p", text: "hello, world")
println(paragraph!.asHTML())
// prints "<p>hello, world</p>
paragraph!.asHTML()
//The asHTML property holds a strong reference to its closure. 
//The closure meanwhile capture self meaning it holds a strong reference to HTMLElement
paragraph = nil
//HTMLElement deinitializer is not printed

/************************************************************************
            Resolving Strong Reference Cycles for Closures
*************************************************************************/

/*
    Place the capture list before a closure's parameter list and return type if they are provided

//e.g.
lazy var someClosure: (Int, String) -> String = {
    [unowned self] (index: Int, stringToProcess: String) -> String in
    // closure body goes here
}

    If a closure does not specify a paremeter list or return type because they can be inferred from context, place the capture list at the very start of the closure followed by the in keyword
//e.g.
lazy var someClosure: () -> String = {
    [unowned self] in
    // closure body goes here
}

 */


//NOTE: If the captured reference will never become nil, it should always be captured as an unowned reference, rather than a weak reference.

class HTMLElement2 {
    let name: String
    let text: String?
    
    lazy var asHTML: () -> String = {
        [unowned self] in
        if let text = self.text {
            return "</\(self.name)>\(text)</\(self.name)>"
        } else {
            return "</\(self.name)>"
        }
    }
    init(name: String, text: String? = nil) {
        self.name = name
        self.text = text
    }
    deinit {
        println("\(name) is being deinitialized")
    }
}

var paragraph2: HTMLElement2? = HTMLElement2(name: "p", text: "hello, world")
println(paragraph2!.asHTML())
paragraph2!.asHTML()
paragraph2 = nil
// prints "p is being deinitialized"

