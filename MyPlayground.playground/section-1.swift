// Playground - noun: a place where people can play

import Cocoa

var str = "Sample"
println("Hello, World");

var myVariable = 42;
myVariable = 50;
let myConstant = 42;

let implicitInteger = 70;
let implicitDouble = 70.0;
let explicitDouble: Double = 70;

//Experiment - the number 4 is converted to float
let explicitFloat: Float = 4;

let label = "The width is "
let width = 94;
let widthLabel = label + String(width)

//Experiment - cannot be added because they are of different type
//let letwidthLabel2 = label + width;

let apples = 3
let oranges = 5
let appleSummary = "I have \(apples) apples"
let fruitSummary = "I have \(apples + oranges) pieces of fruit"

//Experiment
let name = "John Mckein"
println("I have \(5.0 + 6.7) \(name)")

//Arrays and dictionaries
var shoppingList = ["catfish", "water", "tulips", "blue paint"]
shoppingList[1] = "bottle of water"

var occupations = [
    "Malcolm": "Captain",
    "Kaylee": "Mechanic",
]
occupations["Jayne"] = "Public Relations"
occupations

//Creation of empty array or dictionary by using the initializer
let emptyArray = String[]()
let emptyDictionary = Dictionary<String, Float>();

//Control Flow
let individualScores = [75, 43, 103, 87, 12]
var teamScore = 0;
for score in individualScores {
    if score > 50 {
        teamScore += 3
    } else {
        teamScore += 1
    }
}
teamScore

//using if and let together
var optionalString: String? = "Hello"
optionalString == nil //returns boolean

var optionalName: String? = "John AppleSeed"
optionalName = nil;
var greeting = "Hello"
if let name = optionalName {
    greeting += ", \(name)"
} else {
    let name = "Cloud Strife"
    greeting = "Hello, \(name)"
}
greeting

//SWITCH
let vegetable = "red pepper"
switch vegetable {
case "celery":
    let vegetableComment = "Add some raisins and make ants on a log."
case "cucumber", "watercress":
    let vegetableComment = "That would make a good tea sandwich"
case let x where x.hasSuffix("pepper"): //decalare the vegetable to the variable x
    let vegetableComment = "Is it a spicy \(x)?"
default:
    let vegetableComment = "Everything tastes good in soup"
}

//Dictionary of array && for-in
let interestingNumbers = [
    "Prime": [2, 3, 5, 7, 11, 13],
    "Fibonacci": [1, 1, 2, 3, 5, 8],
    "Square": [1, 4, 9, 16, 25],
]

var largest = 0
var kindOfLargest: String? = nil
for (kind, numbers) in interestingNumbers { //intellicense detects if what type is inside the variable
    println(kind) //square is the first one to be printed
    for number in numbers {
        if number > largest {
            largest = number
            kindOfLargest = kind
        }
    }
}
largest
kindOfLargest

//WHILE && DO-LOOP
var n = 2
while n < 100 {
    n = n * 2
}
n

var m = 2
do {
    m = m * 2
} while m < 100
m

//indexing in loop
var firstForLoop = 0
for i in 0..3 { //use ... to include both values
    firstForLoop += i
}
firstForLoop

var secondForLoop = 0
for var i = 0; i < 3; ++i {
    secondForLoop += i
}
secondForLoop

//Functions and Closures
func greet(name: String, lunch: String) -> String {
    return "Hello \(name), today is \(lunch)"
}
greet("Bob", "Adobo")

//Tuple to return multiple values
func getGasPrice() -> (Double, Double, Double) {
    return (3.59, 3.69, 3.79)
}
getGasPrice()

//Function for array
func sumOf(numbers: Int...) -> Int {
    var sum = 0;
    for number in numbers {
        sum += number
    }
    return sum;
}
sumOf()
sumOf(2, 10)

//Experiment: Write a function that calculates the average of its arguments.
//Function for array
func averageOf(numbers: Int...) -> Double {
    var sum = 0;
    for number in numbers {
        sum += number
    }
    return Double(sum) / Double(numbers.count)
}
averageOf(2, 10)

//Functions can be nested
func returnFifteen() -> Int {
    var y = 10
    func add() {
        y += 5
    }
    add()
    return y
}
returnFifteen()

//Functions are a first-class type. This means that a function can return another function as its value.
func makeIncrementer() -> (Int -> Int) {
    func addOne(number: Int) -> Int {
        return 1 + number;
    }
    return addOne //returned this method
}
var increment = makeIncrementer()
increment(7)

//A function can take another function as one of its arguments.
func hasAnyMatches(list: Int[], condition: Int -> Bool) -> Bool {
    for item in list {
        if condition(item) {
            return true
        }
    }
    return false
}
func lessThanTen(number: Int) -> Bool {
    return number < 10
}
var numbers = [20, 19, 7, 12]
hasAnyMatches(numbers, lessThanTen)

func returnListOfDrinks(list: String[]) -> String[] {
    return list
}
var drinks = ["Gatorade", "Yakult", "Royal"]
returnListOfDrinks(drinks)

//Closure functions
numbers.map({
    (number: Int) -> Int in
    let result = 3 * number
    return result
})

//Classes and objects
class Shape {
    var numberOfSides = 0
    let shapeColor = "yellow"
    func simpleDescription() -> String {
        return "A shape with \(numberOfSides) sides"
    }
    func colorOfShape(color: String) -> String {
        return "The color of the shape is \(color)"
    }
}

var shape = Shape()
shape.numberOfSides = 7
var shapeDescription = shape.simpleDescription()
var shapeColor = shape.colorOfShape(shape.shapeColor)

//Class with initializer
class NamedShape {
    var numberOfSides: Int = 0
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func simpleDescription() -> String {
        return "A shape with \(numberOfSides) sides."
    }
    
    func printNameOfShape() {
        println("This is a \(self.name)")
    }
}

var namedShape = NamedShape (name: "Square")
namedShape.printNameOfShape()

//Class with override and subclassing
class Square: NamedShape {
    var sideLength: Double
    
    init(sideLength: Double, name: String) {
        self.sideLength = sideLength //self distinguish
        super.init(name: name)
        numberOfSides = 4
    }
    
    func area() -> Double {
        return sideLength * sideLength
    }
    
    override func simpleDescription() -> String {
        return "A square with the sides of length \(sideLength)"
    }
}

let test = Square(sideLength: 5.2, name: "my test square")
test.area()
test.simpleDescription()

//Experiment: Make another subclass of NamedShape called Circle that takes a radius and a name as arguments to its initializer. Implement an area and a describe method on the Circle class.

class Circle: NamedShape {
    var radius: Double
    
    init(radius: Double, name: String) {
        self.radius = radius
        super.init(name: name)
    }
    
    func area() -> Double {
        return pi * (radius * radius)
    }
    
    func describe() -> String {
        return ("The radius of the circle is \(radius)")
    }
}

var testCircle = Circle(radius: 20.2, name: "My test circle")
testCircle.area()
testCircle.describe()

//Class with getter and setter
class EquilateralTriangle: NamedShape {
    var sideLength: Double = 0.0
    
    init(sideLength: Double, name: String) {
        self.sideLength = sideLength //setting the value of properties that the subclass declares
        super.init(name: name) //calling the superclass initializer
        numberOfSides = 3 //changing the value of properties defined by the superclass
    }
    
    var perimeter: Double {
    get {
        return 3.0 * sideLength
    }
    set {
        sideLength = newValue / 3.0 //new value is the default parameter for the setter
    }
    }
    
    override func simpleDescription() -> String {
        return "An equilateral triangle with sides of length \(sideLength)"
    }

}

var triangle = EquilateralTriangle(sideLength: 3.1, name: "A triangle")
triangle.perimeter
triangle.perimeter = 9.9
triangle.perimeter
triangle.numberOfSides

//willset and didset examples
class TriangleAndSquare {
    var triangle: EquilateralTriangle {
    willSet {
        square.sideLength = newValue.sideLength
    }
    }
    var square: Square {
    willSet {
        triangle.sideLength = newValue.sideLength
    }
    }
    init(size: Double, name: String) {
        square = Square(sideLength: size, name: name)
        triangle = EquilateralTriangle(sideLength: size, name: name)
    }
    
}
var triangleAndSquare = TriangleAndSquare(size: 10, name: "another test shape")
triangleAndSquare.square.sideLength
triangleAndSquare.triangle.sideLength
triangleAndSquare.square = Square(sideLength: 50, name: "larger square")
triangleAndSquare.triangle.sideLength

//Specifying a second name in method
class Counter {
    var count: Int = 0 //explicit
    func incrementby(amount: Int, numberOfTimes times: Int) { //second name times
        count += amount * times
    }
}
var counter = Counter()
counter.incrementby(2, numberOfTimes: 7)

//Optional value
//if the value before the ? is nil everything after ? is ignored and the value of the whole expression is nil
var optionalSquare: Square = Square(sideLength: 2.5, name: "optional square")
optionalSquare.sideLength = 5.0
let sideLength = optionalSquare.sideLength

//You can't assign to the property of the class when the variable is an optional value
var optionalTriangle: EquilateralTriangle? = EquilateralTriangle(sideLength: 2.0, name: "optional triagle")
let sidetriangle = optionalTriangle?.sideLength

//Enumeration and Structures
enum Rank: Int {
    case Ace = 1
    case Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten
    case Jack, Queen, King
    func simpleDescription() -> String {
        switch self {
        case .Ace:
            return "ace"
        case .Jack:
            return "jack"
        case .Queen:
            return "queen"
        case .King:
            return "king"
        default:
            return String(self.toRaw())
            
        }
    }
}
let jack = Rank.Jack //enum value
let jackRawValue = jack.toRaw() //returns the raw value of Jack which is 11
let jackDescription = jack.simpleDescription() // returns "jack"

//Experiment: “Write a function that compares two Rank values by comparing their raw values.”

//Solution 1: By using class method
class ExperimentEnum {
    var rank1: Rank
    var rank2: Rank
    
    init(rank1: Rank, rank2: Rank) {
        self.rank1 = rank1
        self.rank2 = rank2
    }
    
    func compareRanks() -> String {
        if rank1.toRaw() == rank2.toRaw() {
            return "\(rank1.simpleDescription()) is equal to \(rank2.simpleDescription())"
        }
        return "\(rank1.simpleDescription()) is not equal to \(rank2.simpleDescription())"
    }
}

var experimentEnum = ExperimentEnum(rank1: Rank.Jack, rank2: Rank.Jack)
experimentEnum.compareRanks()

//Solution 2: By creating a Function
func compareRanks(rank1: Rank, rank2: Rank) -> String{
    if rank1.toRaw() == rank2.toRaw() {
        return "\(rank1.simpleDescription()) is equal to \(rank2.simpleDescription())"
    }
    return "\(rank1.simpleDescription()) is not equal to \(rank2.simpleDescription())"
}

compareRanks(Rank.Jack, Rank.Queen)

//Using toRaw and fromRaw
if let convertedRank = Rank.fromRaw(3) { //returns a enum value
    let threeDescription = convertedRank.simpleDescription()
}

//Enum without using actual values
enum Suit: Int { //without explicitly assigning a value or type to this enum it is referred to by it's full name
    case Spades, Hearts, Diamonds, Clubs
    func simpleDescription() -> String {
        switch self {
        case .Spades:
            return "spades"
        case .Hearts:
            return "hearts"
        case .Diamonds:
            return "diamonds"
        case .Clubs:
            return "clubs"
        }
    }
    func colorOfSuit() -> String {
        switch self {
        case .Spades, .Clubs:
            return "black"
        case .Hearts, .Diamonds:
            return "red"
        }
    }
}

let hearts = Suit.Hearts
let heartDescription = hearts.simpleDescription()
let heartColor = hearts.colorOfSuit()

//Struct - structures are always copied are always copied when they are passed around your code.

struct Card {
    var rank: Rank
    var suit: Suit
    func simpleDescription() -> String {
        return "The \(rank.simpleDescription()) of \(suit.simpleDescription())"
    }
    //Experiment “Add a method to Card that creates a full deck of cards, with one card of each combination of rank and suit.”
    //Creation of Deck by Suit
    func createDeckBySuit(suit: Suit) {
        for i in 1...13 {
            if let convertedRank = Rank.fromRaw(i) { //check if there's a return value
                let ranDesc = convertedRank.simpleDescription()
                println("The \(ranDesc) of \(suit.simpleDescription())")
            }
            
        }
    }
    
    //Creation of Full Deck of Cards
    //Solution 1
    func createFullDeckOfCards(list: Suit[]) {
        for suit in list {
            createDeckBySuit(suit)
        }
    }
    //Solution 2 - Add int to the deck of cards
    func createFullDeckOfCards() {
        for i in 0...5 {
            if let suit = Suit.fromRaw(i) {
                createDeckBySuit(suit)
            }
        }
    }
    
}
let threeOfSpades = Card(rank: .Three, suit: .Spades)
let threeOfSpadesDescription = threeOfSpades.simpleDescription()
//threeOfSpades.createDeckBySuit(.Spades)
//threeOfSpades.createFullDeckOfCards([.Spades, .Hearts, .Clubs, .Diamonds])
threeOfSpades.createFullDeckOfCards()

enum ServerResponse {
    case Result(String, String)
    case Error(String)
    case Offline(String)
}
let success = ServerResponse.Result("6:00 am", "8:09 pm")
let failure = ServerResponse.Error("Out of cheese.")
let status = ServerResponse.Offline("Your are offline")

switch status { //switch the success constant and reads value passed to serverResponse.Result
case let .Result(sunrise, sunset):
    let serverResponse = "Sunrise is at \(sunrise) and sunset is at \(sunset)"
case let .Error(error):
    let serverResponse = "Failure... \(error)"
case let .Offline(error):
    let ServerResponse = "\(error)"
}


//Protocols and Extensions
protocol ExampleProtocol {
    var simpleDescription: String { get }
    mutating func adjust()
}

class SimpleClass: ExampleProtocol {
    var simpleDescription: String = "A very simple class." //explicit
    var anotherProperty: Int = 69105
    func adjust() {
        simpleDescription += " Now 100% adjusted."
    }
}

var a = SimpleClass()
a.adjust()
let aDescription = a.simpleDescription

struct SimpleStructure: ExampleProtocol {
    var anotherProperty: String = "Sample"
    var simpleDescription: String = "A Simple Structure"
    //we use mutating to mark a method that modifies a structure. Simple class doesn't need to be mutated because methods on a class can always modify a class
    mutating func adjust() {
        simpleDescription += "(adjusted)"
    }
}

var b = SimpleStructure()
b.adjust()
let bDescription = b.simpleDescription

//If you use a protocol you must conform to the protocol methods within your class, enum, etc.
enum simpleEnum: ExampleProtocol {
    case A, B
    var simpleDescription: String {
    get {
        switch self {
        case .A:
            return "ENUM A"
        case .B:
            return "ENUM B"
        }
    }
    }
    mutating func adjust() {
        switch self {
        case .A:
            self = B
        case .B:
            self = B
        }
    }
}

var c = simpleEnum.A
c.simpleDescription
c.adjust()
c.simpleDescription


//Extension
//Use extension to add functionality to an existing type.
extension Int: ExampleProtocol {
    var simpleDescription: String {
        return "The number \(self)"
    }
    mutating func adjust() {
        self += 42
    }
}

7.simpleDescription


//Experiment
//“Write an extension for the Double type that adds an absoluteValue property.”

extension Double {
    var absoluteValue:Double {
        return abs(self)
    }
    
}
let exampleDouble = -40.0
let exampleAbsolutevalue = exampleDouble.absoluteValue

//passing a class to with similar protocol
let protocolValue: ExampleProtocol = a
protocolValue.simpleDescription
//protocolValue.anotherProperty - this will check whether another property exist on the protocol even though it exist in the class in will still follow the protocol

//GENERICS
func repeat<ItemType>(item: ItemType, times: Int) -> ItemType[] { //You can create a generic method and pass a type depending on what type you will used on your method (e.g. Item Type = String, Int, etc)
    var result = ItemType[]()
    for i in 0..times {
        result += item
    }
    return result
}

repeat(6, 4)

//Reimplement the Swift standard Library
enum OptionalValue<T> {
    case None
    case Some(T)
}

var possibleInteger: OptionalValue<Int> = .None
possibleInteger = .Some(1000)


func anyCommonElements <T, U where T: Sequence, U: Sequence, T.GeneratorType.Element: Equatable, T.GeneratorType.Element == U.GeneratorType.Element> (lhs: T, rhs: U) -> Bool {
    for lhsItem in lhs {
        for rhsItem in rhs {
            if lhsItem == rhsItem {
                return true
            }
        }
    }
    return false
}

//EXPERIMENT: “Modify the anyCommonElements function to make a function that returns an array of the elements that any two sequences have in common.”
func anyCommonElements2 <T, U where T: Sequence, U: Sequence, T.GeneratorType.Element: Equatable, T.GeneratorType.Element == U.GeneratorType.Element> (lhs: T, rhs: U) -> Array<T.GeneratorType.Element>{
    func commonElements() -> Array<T.GeneratorType.Element> {
        var array = Array<T.GeneratorType.Element>()
        for lhsItem in lhs {
            for rhsItem in rhs {
                if lhsItem == rhsItem {
                    array.append(lhsItem)
                }
            }
        }
        return array
    }
    return commonElements()
}

anyCommonElements2([1, 2, 3], [3])





